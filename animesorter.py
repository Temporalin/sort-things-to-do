import xml.etree.ElementTree as ET


def get_list_of_animes():
    tree = ET.parse('animelist.xml')
    user = tree.getroot()

    list_of_animes = []
    for anime in user.findall('anime'):
        title = anime.find('series_title').text
        status = anime.find('my_status').text
        if status == 'Plan to Watch':
            list_of_animes.append(title)

    return list_of_animes


def sort_things(things_to_sort, sort_fun, already_sorted=None):
    if already_sorted:
        sorted_list = already_sorted
        rest = things_to_sort
    else:
        first_thing, *rest = things_to_sort
        sorted_list = sorted([first_thing, ])

    for insert_thing in rest:
        inserted = False
        head = 0
        tail = len(sorted_list)

        while not inserted:
            remaining_sorted = sorted_list[head:tail]
            middle_position = head + len(remaining_sorted) // 2
            middle_thing = sorted_list[middle_position]

            if sort_fun(insert_thing, middle_thing):
                tail = middle_position
            else:
                head = middle_position + 1

            inserted = head == tail
            if inserted:
                sorted_list.insert(head, insert_thing)

    return sorted_list


def ask(a, b):
    print('Which one is better?')
    print(f'0) {a}')
    print(f'1) {b}')
    answer = int(input())
    if answer in [0, 1]:
        print()
        return answer == 1
    else:
        raise RuntimeError('bad input is not processed yet, sorry')


def main_mal():
    list_of_animes = get_list_of_animes()[:5]
    sorted_animes = sort_things(list_of_animes, ask)
    for anime in reversed(sorted_animes):
        print(f'- {anime}')


def main_debug():
    res = sort_things(['- z -'], ask, ['a', 'b', 'c', 'd', 'e', 'f'])
    for r in res:
        print(r)


if __name__ == '__main__':
    main_mal()
